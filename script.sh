#!/bin/bash

yum install epel-release -y
yum install jq moreutils -y

customer_name_array=( $(jq -r '.customer[].Name' customer_list.json) )
model_name_array=( $(jq -r '.customer[].Model_Name' customer_list.json) )
model_version_array=( $(jq -r '.customer[].Model_Version' customer_list.json) )
instance_name_array=( $(jq -r '.customer[].Instance_Name' customer_list.json) )
 
k=0
for customer_name in ${customer_name_array[@]} 
do  
    echo "******** Customer related values **************"
    echo customer_name: $customer_name
    echo instance_name: ${instance_name_array[$k]}
    echo model_name: ${model_name_array[$k]}
    echo model_version: ${model_version_array[$k]}
    echo "************************************************************"    
    k=$((k+1))   
done

